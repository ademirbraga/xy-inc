'use strict';

let mongoose = require('mongoose');
let Schema   = mongoose.Schema;
let models = {};
let Resource = mongoose.model('Resources');
let attributes;

/**
 * funcionando
 * @param modelName
 * @returns {*}
 */
exports.getModelByName = function (modelName) {
    if (!models[modelName]) return reject({status: 404, message: 'Rota não encontrada'});
    return models[modelName];
    /*return Resource.findOne({"nome": modelName})
        .then((response) => {
            const attributes = setAttributes(response.fields);
            if (!models[modelName]) {
                models[modelName] = mongoose.model(
                    modelName,
                    new Schema(attributes)
                );
            }
            return models[modelName];
        });
        */
};

exports.createModel = function (modelName, attrs) {
    attributes = setAttributes(attrs);

    models[modelName] = mongoose.model(
        modelName,
        new mongoose.Schema(attributes, {strict: true})
    );
    return models[modelName];
};

/**
 * funcionando
 * @param attrs
 * @returns {{}}
 */
function setAttributes(attrs) {
    let listaAttrs = {};
    for (let i in attrs) {
        if (attrs[i].nome && attrs[i].tipo) {
            listaAttrs[attrs[i].nome] = {
                name: attrs[i].nome,
                type: getType(attrs[i].tipo)
            };
        }
    }
    return listaAttrs;
}

/**
 * funcionando
 * @param type
 * @returns {*}
 */
function getType(type) {
    switch(type.toLowerCase()) {
        case "number":
        case "int":
        case "integer":
        case "float":
        case "double":
        case "decimal":
            return Number;
        case "date":
        case "datetime":
            return Date;
        case "boolean":
        case "bool":
            return Boolean;
        case "array":
            return Array;
        default:
            return String;
    }
}

/**
 * carrrega todos os models
 */
exports.loadModels = function () {
    Resource.find({}, function (err, resources) {
        if (err) res.send(err);
        resources.forEach(function (model, idx) {
            let attrs = setAttributes(model.fields);
            models[model.nome] = mongoose.model(
                model.nome,
                new Schema(attrs)
            );
        });
    });
};
