'use strict';

var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var ResourceSchema = new Schema({
    nome: {
        type: String,
        unique: true,
        required: true
    },
    fields: [{
        nome: {type: String, required: true, unique: false},
        tipo: {type: String, required: true, unique: false}
    }]
});


module.exports = mongoose.model('Resources', ResourceSchema);
