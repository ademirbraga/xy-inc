'use strict';

var mongoose = require('mongoose'),
    Resource = mongoose.model('Resources'),
    helpers  = require('../helpers/models');

exports.getAll = function (req, res) {
    Resource.find({}, function (err, resource) {
        if (err) res.send(err);
        res.json(resource);
    });
};

exports.getOne = function (req, res) {
    Resource.findById(req.params.id, function (err, resource) {
        if (err) res.send(err);
        res.json(resource);
    });
};


exports.createOne = function (req, res) {
    var new_resource = new Resource(req.body);
    new_resource.save(function (err, resource) {
        if (err) res.send(err);
        const model = helpers.createModel(req.body.nome, req.body.fields);
        res.json(resource);
    });
};


exports.update = function (req, res) {
    Resource.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function (err, resource) {
        if (err) res.send(err);
        res.json(resource);
    });
};


exports.deleteOne = function (req, res) {
    Resource.remove({
        _id: req.params.id
    }, function (err, resource) {
        if (err) res.send(err);
        res.json({'message': 'Resource successfully deleted.'});
    });
};
