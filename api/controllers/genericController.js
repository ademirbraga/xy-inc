'use strict';

let mongoose = require('mongoose');
let helpers  = require('../helpers/models');

/**
 * finalizado
 * @param req
 * @param res
 */
exports.get = function (req, res) {
    let model = helpers.getModelByName(req.params.resource);
    if (req.params.id) {
        model.findOne({'_id': req.params.id}, (err, resources) => {
            if (err) res.send(err);
            if (resources == null) res.status(204);
            res.json(resources);
        });
    } else {
        model.find({}, function (err, resources) {
            if (err) res.send(err);
            if (resources == null) res.status(204);
            res.json(resources);
        });
    }
};

/**
 * finalizado
 * @param req
 * @param res
 */
exports.deleteOne = function (req, res) {
    let model = helpers.getModelByName(req.params.resource);
    model.findOne({ _id: req.params.id }, (err, resource) => {
        if (err) res.send(err);
        if (resource == null) res.status(204);

        model.remove({ _id: req.params.id }, function(err) {
            if (err) res.send(err);
            res.json('Resource successfully deleted.');
        });
    });
};


/**
 * finalizado
 * @param req
 * @param res
 */
exports.updateOne = function (req, res) {
    let model = helpers.getModelByName(req.params.resource);
    model.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: false, upsert: false}, (err, resource) => {
        if (err) res.send(err);
        if (resource == null) res.status(204);
        res.json(resource);
    });
};


exports.createOne = function (req, res) {
    let model = helpers.getModelByName(req.params.resource);
    model.create(req.body, function (err, resource) {
        if (err) return res.send(err);
        res.json(resource);
    })
};







