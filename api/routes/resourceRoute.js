'use strict';

module.exports = function (app) {
    var resourceList = require('../controllers/resourceController');

    app.route('/resources')
        .get(resourceList.getAll)
        .post(resourceList.createOne);

    app.route('/resources/:id')
        .get(resourceList.getOne)
        .put(resourceList.update)
        .delete(resourceList.deleteOne);
};
