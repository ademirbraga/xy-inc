'use strict';

module.exports = function (app) {
    var genericList = require('../controllers/genericController');

    app.route('/:resource')
        .get(genericList.get)
        .post(genericList.createOne);

    app.route('/:resource/:id')
        .get(genericList.get)
        .put(genericList.updateOne)
        .delete(genericList.deleteOne);
};
