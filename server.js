var express    = require('express'),
    app        = express(),
    port       = process.env.PORT || 3000,
    mongoose   = require('mongoose'),
    Resource   = require('./api/models/resourceModel'),
    bodyParser = require('body-parser'),
    helpers    = require('./api/helpers/models');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/xy-inc');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
/*app.use(function (req, res) {
    res.status(404).send({url: req.originalUrl+ ' not found'});
});*/

let routes = require('./api/routes/resourceRoute');
routes(app);

let genericRoutes = require('./api/routes/genericRoutes');
genericRoutes(app);

app.listen(port);

console.log('resource RESTFul API server started on: ' + port);

helpers.loadModels();
